
/*!
 *  @mainpage   Tortoise And Hare cycle detection algorithm.
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  A solution to the algorithmic problem of finding a cycle in a sequence
 *  of iterated function values.
 *
 *  For any function ƒ that maps a finite set S to itself,
 *  and any initial value x0 in S, the sequence of iterated function values
 *
 *  x0, x1 = f(x0), x2 = f(x1) ... xi = f(xi-1), ...
 *
 *  must eventually use the same value twice.
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>
#include <cstdlib>

const int F_ARRAY_SIZE = 13;
const int f_array[F_ARRAY_SIZE] = {2, 5, 4, 3, 9, 7, 5, 6, 0, 8, 11, 12, 1};

int f(const int index) {
    return f_array[index];
}

/*!
 *  @brief      Implementation of the Tortoise And Hare algorithm.
 *  @details    Given a function that returns f(xi) and a start index, it
 *              uses the Tortoise And Hare algorithm to detect a cycle.
 *  @param[in, out] firstIndex      Start index to start looking for cycle.
 *  @param[out]     lamda           The length of the cycle.
 *  @param[in]      startIndex      The start index of the cycle.
 */
void tortoiseAndHare(int& firstIndex, int& lamda, const int startIndex)
{
    int tortoise(f(startIndex));
    int hare(f(f(startIndex)));
    lamda = 1;

    // First step is to search for a cycle.
    while(tortoise != hare) {
        std::cout << "[1] Compare [" << tortoise
                  << " : " << hare
                  << "]" << std::endl;

        tortoise = f(tortoise);
        hare = f(f(hare));
    };

    std::cout << "Found cycle[" << tortoise
              << " : " << hare
              << "]" << std::endl;

    // Second step is to find the beginning of the cycle
    tortoise = startIndex;
    while(tortoise != hare) {
        std::cout << "[2] Compare [" << tortoise
                  << " : " << hare
                  << "]" << std::endl;
        tortoise = f(tortoise);
        hare = f(hare);
    };
    firstIndex = hare;

    // Third step is to find the cycle length.
    hare = f(tortoise);
    while(tortoise != hare) {
        std::cout << "[3] Compare [" << tortoise
                  << " : " << hare
                  << "]" << std::endl;
        hare = f(hare);
        lamda++;
    };
}

/*!
 *  @brief      Main function.
 *  @details    Call the algorithm for different starting indexes
 *              and print out the results.
 */
int main(int, char**)
{
    int firstIndex(0);
    int lamda(0);
    tortoiseAndHare(firstIndex, lamda, 2);
    std::cout << "RESULT:\n"
              << "-------\n"
              << "mu    : " << firstIndex << "\n"
              << "lamda : " << lamda << "\n\n" << std::endl;

    tortoiseAndHare(firstIndex, lamda, 4);
    std::cout << "RESULT:\n"
              << "-------\n"
              << "mu    : " << firstIndex << "\n"
              << "lamda : " << lamda << "\n\n" << std::endl;

    tortoiseAndHare(firstIndex, lamda, 9);
    std::cout << "RESULT:\n"
              << "-------\n"
              << "mu    : " << firstIndex << "\n"
              << "lamda : " << lamda << "\n\n" << std::endl;
    return EXIT_SUCCESS;
}

