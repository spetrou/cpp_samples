
/*!
 *  @mainpage   Reverse order of words in a string
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Given a string, reverse the order of the words.
 *  Very common interview question. There's a naive/quick
 *  approach of tokenizing the string and then puting the
 *  words back together in the reverse order.
 *  They will usually ask for the in-place approach where
 *  you reverse the entire string in the first step and then
 *  you reverse each word. Both implementations are shown below.
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <cstdlib>

/*!
 *  @brief      Reverse the order of words.
 *  @details    Reverse the order of the words in the given string using
 *              tokenizing.
 *  @param[in]  theString   The string to reverse its words order.
 */
void reverse_with_tokens(std::string& theString)
{
    std::vector<std::string> tokens;
    std::stringstream ss(theString);
    std::string localString;
    while(ss >> localString) {
        tokens.push_back(localString);
    }

    ss.str("");
    ss.clear();
    for(std::vector<std::string>::const_reverse_iterator it = tokens.rbegin(),
                                                         end = tokens.rend();
        it != end; ++it)
    {
        ss << (*it) << " ";
    }
    theString = ss.str();
}

/*!
 *  @brief      Reverse the order of words.
 *  @details    Reverse the order of words in the given string in-place.
 *              First it reverses the entire string, and then each work
 *              separately.
 *  @param[in]  theString   The string to reverse its words order.
 */
void reverse_in_place(std::string& theString)
{
    size_t left = 0, right;
    if(theString.length() == 0 ) {
        return;
    }

    // Reverse the entire string first
    right = theString.length() - 1;
    while(left < right) {
        char localTemp = theString.at(left);
        theString.at(left) = theString.at(right);
        theString.at(right) = localTemp;
        --right;
        ++left;
    }

    // Find the spaces between words and reverse each individual word
    left = 0;
    while(left < theString.length()) {
        right = theString.find_first_of(' ', left+1);

        if(right == std::string::npos) {
            right = theString.length() - 1;
        } else {
            --right;
        }

        size_t newLeft = right + 2;
        while(left < right) {
            char localTemp = theString.at(left);
            theString.at(left) = theString.at(right);
            theString.at(right) = localTemp;
            --right;
            ++left;
        }

        left = newLeft;
    }
}

/*
 *  @brief      Main function
 *  @details    Pass a string to both functions and print out the result of
 *              the word order reversal.
 */
int main(int, char**)
{
    std::string theString("A red bull");
    std::cout << "Before reverse_with_tokens: " << theString << std::endl;
    reverse_with_tokens(theString);
    std::cout << "After reverse_with_tokens : " << theString << "\n" << std::endl;

    theString = "A red bull";
    std::cout << "Before reverse_in_place: " << theString << std::endl;
    reverse_in_place(theString);
    std::cout << "After reverse_in_place : " << theString << "\n" << std::endl;


    // Single word example
    theString = "singleton";
    std::cout << "Before reverse_with_tokens: " << theString << std::endl;
    reverse_with_tokens(theString);
    std::cout << "After reverse_with_tokens : " << theString << "\n" << std::endl;

    theString = "singleton";
    std::cout << "Before reverse_in_place: " << theString << std::endl;
    reverse_in_place(theString);
    std::cout << "After reverse_in_place : " << theString << "\n" << std::endl;

    return EXIT_SUCCESS;
}

