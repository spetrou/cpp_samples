
/*!
 *  @mainpage   Print nested tuples
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Write a function that can print a nested tuples using the 'std::pair' type.
 *  The innermost tuple will contain an instance of a struct 'ABC' as its
 *  second value.
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>
#include <cstdlib>

/*!
 *  @class  ABC
 *  @brief  An empty test class.
 */
struct ABC {
};

/*!
 *  @brief      Print both values of the a pair.
 *  @details    Given a 'pair' of values, print both of them
 *              to standard output, given that they can be
 *              streamable.
 *              The 'second' value of the pair can be another
 *              another 'pair' or an instance of the 'ABC' struct.
 *  @param[in]  thePair     The pair to print out.
 */
template <typename T, typename K>
void print_pair(const std::pair<T, K>& thePair)
{
    std::cout << thePair.first << std::endl;

    // My initial thought was to add a runtime type checking
    // code at this point and terminate/return when the type
    // of the 'second' was an 'ABC' class. This approach will
    // not work because the compiler cannot resolve that during
    // compilation.
    // The solution is to declare the function below. It will
    // be used for the innermost call of the nested tuple and
    // the recursion will end there.
    print_pair(thePair.second);
}

/*!
 *  @brief      An overloaded version of the 'print_pair' function.
 *  @details    This function is needed for the innermost call of
 *              the templatized function above. When the template
 *              is expanded during compilation, the last call to
 *              'print_pair' will be done on an 'ABC' object. If
 *              we don't declare this function the compiler will not
 *              be able to resolve the last call and fail to compile.
 *  @param[in]  obj     The instance of ABC to print out.
 */
void print_pair(const ABC& obj) {
    std::cout << "ABC obj\n" << std::endl;
}

/*
 *  @brief      Main function
 *  @details    Create nested pairs and print them using 'print_pair'.
 */
int main(int, char**)
{
    std::pair<double,
        std::pair<std::string,
            std::pair<int, ABC> > > myTuple
                = std::make_pair(12.12,
                    std::make_pair("savvas",
                        std::make_pair(12, ABC())));

    print_pair(myTuple);


    std::pair<double,
        std::pair<std::string,
            std::pair<int,
                std::pair<std::string, ABC> > > > myTuple1
                    = std::make_pair(12.12,
                        std::make_pair("savvas",
                            std::make_pair(12, 
                                std::make_pair("another", ABC()))));

    print_pair(myTuple1);

    return EXIT_SUCCESS;
}

