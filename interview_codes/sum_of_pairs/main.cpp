
/*!
 *  @mainpage   Scan an array of numbers and find at least two
 *              pairs that sum up to a given value.
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Given an array of numbers scan the array for how many pairs of
 *  values the sum is equal to the one given as input to the function.
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>
#include <vector>
#include <algorithm>

// #define DEBUG

/*!
 *  @brief      Check if at least two pairs sum up to the constant.
 *  @details    Non-optimal solution. Iterates over all pairs and checks
 *              the sum. This takes O(n^2).
 *  @param[in]  vectorOfNumbers     Vector of numbers.
 *  @param[in]  c                   Constant to check.
 *  @return True if at least two pairs sum up to the constant, false otherwise.
 */
bool has_at_least_two_pairs_1(const std::vector<int>& vectorOfNumbers, const int c)
{
    unsigned int foundCounter(0);
    for(int i = 0; i < vectorOfNumbers.size(); ++i) {
        for(int j = i; j < vectorOfNumbers.size(); ++j) {
            if(vectorOfNumbers[i] + vectorOfNumbers[j] == c) {
                ++foundCounter;
#ifdef DEBUG
            std::cout << "Pair [" << vectorOfNumbers[i] << "] ["
                      << c - vectorOfNumbers[i] << "]" << std::endl;
#endif
            }
        }
    }

    if(foundCounter > 1) {
        return true;
    } else {
        return false;
    }
}

/*!
 *  @brief      Check if at least two pairs sum up to the constant.
 *  @details    Optimized solution. Sort the array at the beginning.
 *              Then search for each number if the remainder of the
 *              subtraction of the number from the constant exists.
 *              If yes, then the pair will sum up to the constant.
 *              This is O(nlogn) for sort, and O(nlogn) for searching.
 *  @param[in]  vectorOfNumbers     Vector of numbers.
 *  @param[in]  c                   Constant to check.
 *  @return True if at least two pairs sum up to the constant, false otherwise.
 */
bool has_at_least_two_pairs_2(const std::vector<int>& vectorOfNumbers, const int c)
{
    std::vector<int> localCopy(vectorOfNumbers);
    std::sort(localCopy.begin(), localCopy.end());

    unsigned int foundCounter(0);
    for(int i = 0; i < localCopy.size(); ++i) {
        // The vector is sorted and with an optimized search algorithm
        // (for example binary search) you can find if the complementary
        // value exists in the vector in O(logn)
        if(std::binary_search(localCopy.begin()+i, localCopy.end(), c - localCopy[i])) {
#ifdef DEBUG
            std::cout << "Pair [" << localCopy[i] << "] ["
                      << c - localCopy[i] << "]" << std::endl;
#endif
            ++foundCounter;
        }
    }

    if(foundCounter > 1) {
        return true;
    } else {
        return false;
    }
}

/*!
 *  @brief  Main function.
 */
int main()
{
    std::vector<int> vectorOfNumbers;
    const int constantValue(34);

    vectorOfNumbers.push_back(-42);
    vectorOfNumbers.push_back(-6);
    vectorOfNumbers.push_back(27);
    vectorOfNumbers.push_back(4);
    vectorOfNumbers.push_back(23);
    vectorOfNumbers.push_back(65);
    vectorOfNumbers.push_back(-10);
    vectorOfNumbers.push_back(30);
    vectorOfNumbers.push_back(76);
    vectorOfNumbers.push_back(9);

    std::cout << "Does it have at least two pairs: "
              << (has_at_least_two_pairs_1(vectorOfNumbers, constantValue)? "YES":"NO")
              << std::endl;

    std::cout << "Does it have at least two pairs: "
              << (has_at_least_two_pairs_2(vectorOfNumbers, constantValue)? "YES":"NO")
              << std::endl;
}

