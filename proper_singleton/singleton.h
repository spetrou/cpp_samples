
#include <iostream>

/// @class  Manage the lifecycle of the singleton.
class SingletonGuard {
public:
    SingletonGuard();
        /// @brief  Attach (allocate) an instance of the singleton.

    ~SingletonGuard();
        /// @brief  Detach (deallocate) the instance of the singleton.
};

class Singleton {
private:
    int d_int;

    /// Protect the constructors and the destructor so that only
    /// the guard can use them. No other instance of this class
    /// needs to be initialized.

    Singleton()
    : d_int(10)
    { }
        /// @brief  Default constructor.

    Singleton(int const in)
    : d_int(in)
    { }
        /// @brief  Value constructor.

    ~Singleton()
    { }
        /// @brief  Virtual singleton.

    friend class SingletonGuard;
        /// @brief  Only the guard can construct a concrete instance.

public:

    Singleton& operator*() {
        return *this;
    }

    Singleton* operator->() {
        return this;
    }

    void aFunc() {
        std::cout << "[BASE] Internal int ["
                  << d_int
                  << "]"
                  << std::endl;
    }

    static Singleton* instance();
};
