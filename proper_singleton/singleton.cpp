#include "singleton.h"
#include <cassert>


Singleton *theMainInstance = nullptr;

Singleton* Singleton::instance() {
    assert(theMainInstance != nullptr);
    return theMainInstance;
}


SingletonGuard::SingletonGuard() {
    assert(theMainInstance == nullptr);
    std::cout << "Singleton [ACTIVE]" << std::endl;
    theMainInstance = new Singleton();
}

SingletonGuard::~SingletonGuard() {
    assert(theMainInstance != nullptr);
    delete theMainInstance;
    std::cout << "Singleton [INACTIVE]" << std::endl;
    theMainInstance = nullptr;
}

