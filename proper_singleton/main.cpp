#include<iostream>
#include<cstdlib>

#include "singleton.h"

/// @brief  Implementation of a singleton that we have actual control
///         over the lifecycle of the object it manages. Both construction
///         and destruction.
///         In usual implementations you can choose when to instantiate
///         the singleton but deallocation is really tricky

int main(int, char**) {

    // Allocate the singleton guaard
    SingletonGuard *theGuard = new SingletonGuard();

    Singleton::instance()->aFunc();

    // Not visible for public use
//    Singleton *another = new Singleton; // ERROR

    // Deallocate the guard and subsequently the singleton
    delete theGuard;

    return EXIT_SUCCESS;
}

