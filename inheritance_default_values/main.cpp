
/*!
 *  @mainpage   Inheritance - Default function values
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Demonstrates that default values are read from the base
 *  class and not from the derived classes.
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>

/*!
 *  @class  Foo
 *  @brief  Foo base class.
 */
struct Foo
{
    /*!
     *  @brief  Pure virtual base function.
     *  @param[in]  st  An integer.
     */
    virtual size_t func(size_t st = 11) const = 0;
};

/*!
 *  @class  Bar
 *  @brief  Bar derived class.
 */
struct Bar : Foo
{
    /*!
     *  @brief      Virtual function implementation.
     *  @details    Prints out the input value of the parameter if
     *              it's not initialized and the default value is used.
     *  @param[in]  st  An integer.
     */
    virtual size_t func(size_t st = 999) const
    {
        return st;
    }
};

/*!
 *  @brief  Main function.
 */
int main()
{
    Foo const & foo = Bar();
    size_t st = foo.func(); // What value does st have?

    // On return st will be assigned 11 and not, as one
    // would intuitively expect, 999. The reason for this
    // is because default parameters are always bound to
    // the static and NOT dynamic type.
    std::cout << "Value: " << st << std::endl;
}

