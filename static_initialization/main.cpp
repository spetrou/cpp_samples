

/*!
 *  @mainpage   Static variable initialization.
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Local 'static' variables in functions are initialized on
 *  first call to the function and then retain their valus..
 *
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>
#include <stdlib.h>

/*!
 *  @brief      Function with local 'static' variable.
 *  @details    For example the code below will look like this:
 *
 *  @code{.cpp}
 *      void go( int x ) {
 *          static int j;
 *          static bool j_initialized;
 *
 *          if (!j_initialized) {
 *              j = x;
 *              j_initialized = true;
 *          }
 *          ...
 *      }
 *  @endcode
 *  @param[in]  x   An input parameter.
 */
void go(int x)
{
    static int j = x ;
    std::cout << ++j << std::endl ; // see 6, 7, 8
} 

/*!
 *  @brief      Main function.
 *  @details    Initializes the local 'static' variable of a
 *              function and then calls it a few times.
 *  @return     The exit code.
 */
int main(int argc, char** argv)
{
    go(5) ;
    go(5) ;
    go(5) ; 

    return EXIT_SUCCESS;
}

