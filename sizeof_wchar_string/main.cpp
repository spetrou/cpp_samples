#include <iostream>

/*!
 *  @brief      Main function.
 *  @details    Prints out the size of a wchar_t (wide character).
 */
int main(int, char**)
{
    std::cout << "sizeof(L\"Hello World\") : " << sizeof(L"Hello World") << std::endl;
    std::cout << "12 * sizeof(wchar_t) : " << 12*sizeof(wchar_t) << std::endl;
}

