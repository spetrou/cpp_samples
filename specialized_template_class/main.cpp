
/*!
 *  @mainpage   Class specialization
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Demonstrate templetized class specializations.
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>
#include <stdlib.h>

/*!
 *  @class  myClass
 *  @brief  Generic class definition.
 */
template<class T, int length>
class myClass {
    const std::string d_name;
public:
    myClass(const std::string name): d_name(name) {}
    void func(void) { std::cout << d_name << std::endl; }
};

/*!
 *  @class  myClass
 *  @brief  Specialized.
 */
template<int length>
class myClass<bool, length> {
    const std::string d_name;
public:
    myClass(const std::string name): d_name("Specialized: " + name) {}
    void func(void) { std::cout << d_name << std::endl; }
};

/*!
 *  @class  myClass
 *  @brief  Even more specialized.
 */
template<>
class myClass<int, 15> {
    const std::string d_name;
public:
    myClass(const std::string name): d_name("Whooper: " + name) {}
    void func_diff(void) { std::cout << d_name << std::endl; }
};

/*
 *  @brief      Main function
 *  @details    Create a few pointers and perform arithmetic operations
 *              on them.
 */
int main(int argc, char** argv)
{
    myClass<bool, 20> test1("Bool");
    myClass<int, 10> test2("Integer");
    myClass<int, 15> test3("Integer");

    std::cout << "This is a bloody test" << std::endl;
    test1.func();
    test2.func();
    test3.func_diff();

    return EXIT_SUCCESS;
}

