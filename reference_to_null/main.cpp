
/*!
 *  @mainpage   Reference to NULL
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  It is allowed to assign a reference to point to a NULL
 *  initialized pointer. Below is an example of this and
 *  what are the implications.
 *  This example also demostrates an issue with assigning
 *  references to pointer instances.
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>
#include <stdlib.h>

int main(int, char**)
{
    /*
     *  The 'reference' is *locked* to the address of the pointer.
     *  References are not allowed to change the address they are
     *  pointing at. Which means when the pointer is free'd or
     *  point to a different location the reference will not follow.
     *  This might cause issues when the memory is re-allocated to
     *  another variable and the reference is still thought to be
     *  valid.
     */
    int *anInt_2 = new int(3);
    int &theRef_2 = *anInt_2;
    std::cout << "[Before 'delete'] The address of the pointer: "
              << anInt_2 << std::endl;
    std::cout << "[Before 'delete'] Is the reference null? "
              << (&theRef_2 == NULL) << std::endl;
    std::cout << "[Before 'delete'] The address the reference is locked to: "
              << &theRef_2 << std::endl;
    if(&theRef_2 != NULL) {
        std::cout << "[Before 'delete'] The value of ref: "
                  << theRef_2 << std::endl;
    }
    delete anInt_2;
    anInt_2 = NULL;
    // Reference is 'broken' from here onwards.
    std::cout << "[After 'delete']  The address of the pointer: "
              << anInt_2 << std::endl;
    std::cout << "[After 'delete']  Is the reference null? "
              << (&theRef_2 == NULL) << std::endl;
    std::cout << "[After 'delete']  The address the reference is locked to: "
              << &theRef_2 << std::endl;
    if(&theRef_2 != NULL) {
        std::cout << "[After 'delete']  The value of ref: "
                  << theRef_2 << std::endl;
    }


    /*
     *  If the initial address of the pointer is NULL, then the
     *  reference will point to a NULL address which will cause
     *  a segmentation fault if is used.
     */
    std::cout << "\n\n" << std::endl;
    int *anInt = NULL;
    int &theRef = *anInt;
    std::cout << "The address of the pointer: " << anInt << std::endl;
    std::cout << "Is the reference null? " << (&theRef == NULL) << std::endl;
    if(&theRef != NULL) {
        std::cout << "The value of ref: " << theRef << std::endl;
    }

    return EXIT_SUCCESS;
}

