
/*!
 *  @mainpage   Empty class sizeof
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Demonstrate the sizeof an empty class object and how it
 *  varies as more things are added to it.
 */

/*!
 *  @file   main.cpp
 */

#include <stdlib.h>
#include <iostream>

/*!
 *  @class  emptyClass
 *  @brief  emptyClass class.
 */
class emptyClass {
};

/*!
 *  @class  emptyClassFunc
 *  @brief  emptyClassFunc class.
 */
class emptyClassFunc {
    int foo(void) {}
};

/*!
 *  @class  emptyVirtualClass
 *  @brief  emptyVirtualClass class.
 */
class emptyVirtualClass {
public:
    virtual int foo(void) {}
};

/*!
 *  @class  variableClass
 *  @brief  variableClass class.
 */
class variableClass {
public:
    int d_anInt;
};

/*
 *  @brief  Function to test pointer size.
 *  @return An integer.
 */
int testFunc(void) { }

int (* myFunc)(void) = &testFunc;

/*
 *  @brief      Main function
 *  @details    Create different instances of classes and get their
 *              size.
 */
int main(int, char**)
{

    emptyClass class_one;
    emptyVirtualClass class_two;
    emptyClassFunc class_three;
    variableClass class_four;

    std::cout << "Empty class: " << sizeof(emptyClass) << std::endl;
    std::cout << "Empty class instance: " << sizeof class_one << std::endl;
    std::cout << std::endl;

    std::cout << "Empty virtual class: "
              << sizeof(emptyVirtualClass) << std::endl;
    std::cout << "Empty virtual class instance: "
              << sizeof class_two << std::endl;
    std::cout << std::endl;

    std::cout << "Empty class with function: "
              << sizeof(emptyClassFunc) << std::endl;
    std::cout << "Empty class with function instance: "
              << sizeof class_three << std::endl;
    std::cout << std::endl;

    std::cout << "Class with variable: "
              << sizeof(variableClass) << std::endl;
    std::cout << "Class with variable instance: "
              << sizeof class_four << std::endl;
    std::cout << std::endl;

    std::cout << "Size of int: " << sizeof(int) << std::endl;
    std::cout << "Size of func pointer: " << sizeof myFunc << std::endl;

    return EXIT_SUCCESS;
}

