
/*!
 *  @mainpage   Queue Push function.
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  How to correctly implement the 'push' function of a queue function.
 *  Most times the wrong parameter is passed in and the results we
 *  expect to make are not implemented.
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>
#include <stdlib.h>

/*!
 *  @class  node
 *  @brief  Simple node class.
 */
struct node {
    int data;
    struct node* next;
};

/*!
 *  @brief      Function that shows the broken implementation.
 *  @details    Accepts the head as a pointer. What this does is pass
 *              a 'value' copy of the pointer value. When you change
 *              the address where the pointer points, the change does
 *              not remain but gets lost after the end of the function.
 *  @param[in,out]  head    The head of the queue.
 *  @param[in]      data    The new node data.
 *  @see    node
 */
void Push_broken(struct node* head, int data)
{
    struct node* newNode = (struct node*)malloc(sizeof(struct node));
    newNode->data = data;
    newNode->next = head;
    std::cout << "[PUSH] Head before:" << head << std::endl;
    head = newNode;
    std::cout << "[PUSH] Head after:" << head << std::endl;
}

/*!
 *  @brief      Function that shows the correct implementation.
 *  @details    Accepts the head as a pointer to a pointer.
 *  @param[in,out]  head    The head of the queue.
 *  @param[in]      data    The new node data.
 *  @see    node
 */
void Push(struct node** head, int data)
{
    struct node* newNode = (struct node*)malloc(sizeof(struct node));
    newNode->data = data;
    newNode->next = *head;
    std::cout << "[PUSH] Head before:" << *head << std::endl;
    *head = newNode;
    std::cout << "[PUSH] Head after:" << *head << std::endl;
}

/*!
 *  @brief      Function that uses the wrong implementation.
 *  @details    Creates a queue and then prints it out.
 */
void use_broken(void)
{
    struct node* head = NULL;

    std::cout << "Head before:" << head << std::endl;
    Push_broken(head, 150);
    std::cout << "Head after:" << head << std::endl;
    Push_broken(head, 230);
    std::cout << "Head after:" << head << std::endl;

    struct node* iter = head;
    std::cout << "\n" << std::endl;
    while(iter) {
        std::cout << "Value: " << iter->data << std::endl;
        iter = iter->next;
    }
}

/*!
 *  @brief      Function that uses the correct implementation.
 *  @details    Creates a queue and then prints it out.
 */
void use_fixed(void)
{
    struct node* head = NULL;

    std::cout << "Head before:" << head << std::endl;
    Push(&head, 150);
    std::cout << "Head after:" << head << std::endl;
    Push(&head, 230);
    std::cout << "Head after:" << head << std::endl;

    struct node* iter = head;
    std::cout << "\n" << std::endl;
    while(iter) {
        std::cout << "Value: " << iter->data << std::endl;
        iter = iter->next;
    }
}

/*!
 *  @brief      Main function.
 *  @details    Use the broken and correct implemenation of a function
 *              that pushes a node element into a queue.
 */
int main(int, char**)
{
    use_broken();
    std::cout << "\n\n" << std::endl;
    use_fixed();
}

