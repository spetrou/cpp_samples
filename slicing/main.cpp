
/*!
 *  @mainpage   Inheritance - Class slicing
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  How you pass a derived class into a function can sometimes
 *  have unexpected results. In this code the same function is
 *  declared twice. The first accepting a parameter by value, and
 *  the second accepting a parameter by reference. The second
 *  function is able to call the function from the derived class
 *  while the first function only gets the base part of the derived
 *  class (slice) and calls the base class' function.
 */

/*!
 *  @file   main.cpp
 */
#include <iostream>
#include <stdlib.h>

/*!
 *  @class  Base
 *  @brief  Base class.
 */
class Base {
public:
    /*!
     *  @brief  Virtual function to print out something.
     */
    virtual void printSomething(void) const {
        std::cout << "BASE" << std::endl;
    }
};

/*!
 *  @class  Derv
 *  @brief  Derived class.
 */
class Derv : public Base {
public:
    /*!
     *  @brief  Virtual function in the derived class that prints
     *          something different than the base class.
     */
    virtual void printSomething(void) const {
        std::cout << "DERV" << std::endl;
    }
};

/*!
 *  @brief      Function that exibits slicing pitfall.
 *  @details    Accepts a parameter by-value.
 *  @param[in]  obj     A Base class object.
 *  @see    noSlicing
 */
void slicing(Base obj) {
    obj.printSomething();
}

/*!
 *  @brief      Function that does *NOT* exibit slicing.
 *  @details    Accepts a parameter by-reference.
 *  @param[in]  obj     A Base class object reference.
 *  @see    slicing
 */
void noSlicing(const Base& obj) {
    obj.printSomething();
}

int main(int argc, char** argv)
{

    Base baseObj;
    Derv dervObj;

    slicing(baseObj);
    slicing(dervObj);

    noSlicing(baseObj);
    noSlicing(dervObj);

    return EXIT_SUCCESS;
}

