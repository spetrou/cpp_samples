
/*!
 *  @mainpage   
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Exhibits how parameter promotion can affect the function that
 *  is been chosen to be executed.
 */

/*!
 *  @file   main.cpp
 */



#include <iostream>
#include <stdlib.h>

/*!
 *  @class      first
 *  @brief      Base class definitiion.
 *  @details    Declares and defines a function to print out
 *              the value of the input parameter.
 */
struct first {
    /*!
     *  @brief  Print to standard output the value of the input
     *          parameter.
     *  @param[in]  a   The input parameter.
     */
    void f(const int a) {
        std::cout << "An int" << std::endl;
    }
};

/*!
 *  @class      second
 *  @brief      Derived class definitiion.
 *  @details    Declares and defines a similar function to the Base
 *              class but with a different type of input paramter.
 */
struct second : public first {
    /*!
     *  @brief  Print to standard output the value of the input
     *          parameter.
     *  @param[in]  a   The input parameter.
     */
    void f(const double a) {
        std::cout << "A double" << std::endl;
    }
};

/*!
 *  @brief      Main function.
 *  @details    Creates two objects, one of the Base and one of the
 *              Derived class and call the same function on both of
 *              them.
 *  @return     The exit code.
 */
int main(int argc, char**argv)
{
    double aDouble(1.0);
    int anInt(1);

    first one;
    one.f(anInt);
    one.f(aDouble);

    second two;
    two.f(anInt);
    two.f(aDouble);

    return EXIT_SUCCESS;
}

