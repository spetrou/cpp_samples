
/*!
 *  @mainpage   
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Shows the use of the keyword 'typename'.
 */

/*!
 *  @file   main.cpp
 */


#include <iostream>
#include <stdlib.h>
#include <vector>

/*!
 *  @brief  Prints to standard output the second element of the input parameter.
 *  @param[in]  obj     The object.
 */
template <typename T>
void print2nd(const T& obj)
{
    if(obj.size() >= 2) {

        // g++ will complain if the 'typename' is ommited.
        // This is a depended typename and the compiler by default will
        // parse it as a non-typename.
        typename T::const_iterator iter(obj.begin());
        //T::const_iterator iter(obj.begin());
        ++iter;
        std::cout << "The value of 2nd: [" << (*iter) <<"]" << std::endl;
    }
}

/*!
 *  @brief      Main function.
 *  @details    Creates an object, pushes two elements into it and calls
 *              a function to print out the second element.
 *  @return     The exit code.
 */
int main(int argc, char** argv)
{
    std::vector<int> intVec;

    intVec.push_back(10);
    intVec.push_back(20);

    print2nd(intVec);

    return EXIT_SUCCESS;
}

