
/*!
 *  @mainpage   
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Exposes an issue with value promotion even when the constructor
 *  of a 'class' is declared as 'explicit'.
 *
 *  The 'promotion' or 'demotion' of a parameter is still eligible.
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>
#include <stdlib.h>

/*!
 *  @class      first
 *  @brief      Class definitiion.
 *  @details    Declares and defines an explicit constructor.
 */
struct first {
    /*!
     *  @brief      Explicit constructor.
     *  @details    Construct the class from an integer.
     *  @param[in]  a   An integer.
     */
    explicit first(const int a) {
        std::cout << "An int" << std::endl;
    }
};

/*!
 *  @brief      Main function.
 *  @details    Create a class object calling its constructor with
 *              the 'wrong' parameter type to expose the value
 *              promotion even when 'explicit' is used.
 *  @return     The exit code.
 */
int main(int argc, char**argv)
{
    double theValue(1.0);
    first one(theValue);

    return EXIT_SUCCESS;
}

