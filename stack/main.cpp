
/*!
 *  @mainpage   Stack implementation.
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Implementation of a simple stack class.
 *  The class uses templates and demonstrates how it can be used
 *  with POD data and also user defined objects.
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>
#include <stdexcept>
#include <stdlib.h>

/*!
 *  @class  Node
 *  @brief  Class to hold an element of the Stack and the pointer
 *          to the previous element.
 */
template<class T>
class Node {
private:
    T* d_element;
    Node* d_previous;
public:
    /*!
     *  @brief      Default constructor.
     *  @details    Initializes the internal pointers to NULL.
     */
    Node() {
        d_element = NULL;
        d_previous = NULL;
    };

    /*!
     *  @brief      Element constructor.
     *  @details    Initializes the internal pointer to the element
     *              passed in.
     *  @param[in]  element The element to add to the Node.
     */
    Node(const T& element) {
        d_element = new T(element);
    }

    /*!
     *  @brief      Default destructor.
     *  @details    Deallocates the internal pointer if it's initialized.
     */
    ~Node() {
        if(d_element) {
            delete[] d_element;
        }
    }

    /*!
     *  @brief      Set the pointer to the previous element.
     *  @details    Sets the internal pointer to the previous element to the
     *              address of the passed in element.
     *  @param[in]  previous    Element to link to 'previous' internal pointer.
     */
    void setPrevious(Node* previous) {
        d_previous = previous;
    }

    /*!
     *  @brief      Retrieve the pointer to the previous element.
     *  @details    Returns the internal pointer to the previous element.
     *  @return Pointer to 'previous' element.
     */
    Node* getPrevious(void) {
        return d_previous;
    }

    /*!
     *  @brief      Retrieve the element of the Node.
     *  @details    Returns a copy of the element kept by this Node.
     *  @return Copy of element kept by this Node.
     */
    T getElement(void) {
        return *d_element;
    }
};

/*!
 *  @class  Stack
 *  @brief  Class implementing the actual Stack.
 */
template<class T>
class Stack {
private:
    Node<T>* d_tail;
    unsigned int d_size;
public:
    /*!
     *  @brief      Default constructor.
     *  @details    Initializes the internal pointers and values.
     */
    Stack() {
        d_tail = NULL;
        d_size = 0;
    };

    /*!
     *  @brief      Push element into the Stack.
     *  @details    Pushes an element at the 'top' of the Stack.
     *  @param[in]  element The element to push.
     */
    void push(const T& element) {
        Node<T>* newNode = new Node<T>(element);
        if(d_tail) {
            newNode->setPrevious(d_tail);
        }
        d_tail = newNode;
        ++d_size;
    }

    /*!
     *  @brief      Return size of the Stack.
     *  @details    Return how many elements are kept by this Stack.
     *  @return The size (element count) of the Stack.
     */
    const unsigned int size(void) const {
        return d_size;
    }

    /*!
     *  @brief      Pop an element from the Stack.
     *  @details    Return the last pushed element of the stack Stack (LIFO).
     *  @return The last pushed element of the Stack. Throws on empty list.
     */
    T pop(void) throw(std::runtime_error) {
        if(!d_tail) {
            throw std::runtime_error("Empty list");
        }
        T theElement = d_tail->getElement();
        Node<T>* previous = d_tail->getPrevious();
        delete d_tail;
        d_tail = previous;
        --d_size;
        return theElement;
    }
};

/*!
 *  @class  Mine
 *  @brief  Test class.
 */
class Mine {
public:
    /*!
     *  @brief  Print something to std::cout.
     *  @param[in,out]  os      Stream to print out.
     *  @param[in]      element Element to print out.
     *  @return The stream. Useful for chain commands.
     */
    friend std::ostream& operator<<(std::ostream& os, const Mine& element) {
        os << "Test";
        return os;
    }
};

/*!
 *  @brief  Main function.
 */
int main(int argc, char** argv)
{
    Stack<int> myStack;

    myStack.push(10);
    myStack.push(40);
    myStack.push(30);
    myStack.push(50);
    myStack.push(1100);

    try {
        for(int i = 0; i < 10; ++i) {
            std::cout << "Pop: " << myStack.pop() << std::endl;
            std::cout << "Size: " << myStack.size() << std::endl;
        }
    } catch(const std::runtime_error& e) {
        std::cout << "exception: " << e.what() << std::endl;
    }


    Stack<char> myStack_char;

    myStack_char.push('h');
    myStack_char.push('d');
    myStack_char.push('G');
    myStack_char.push('2');
    myStack_char.push('V');


    try {
        for(int i = 0; i < 10; ++i) {
            std::cout << "Pop: " << myStack_char.pop() << std::endl;
            std::cout << "Size: " << myStack_char.size() << std::endl;
        }
    } catch(const std::runtime_error& e) {
        std::cout << "exception: " << e.what() << std::endl;
    }


    Stack<Mine> myStack_mine;

    myStack_mine.push(Mine());
    myStack_mine.push(Mine());
    myStack_mine.push(Mine());
    myStack_mine.push(Mine());
    myStack_mine.push(Mine());


    try {
        for(int i = 0; i < 10; ++i) {
            std::cout << "Pop: " << myStack_mine.pop() << std::endl;
            std::cout << "Size: " << myStack_mine.size() << std::endl;
        }
    } catch(const std::runtime_error& e) {
        std::cout << "exception: " << e.what() << std::endl;
    }

    return EXIT_SUCCESS;
}

