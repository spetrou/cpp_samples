#include<iostream>
#include<cstdlib>
#include <functional>

/// @brief  Sample code to demo that the 'bind' result will accept
///         any number of arguments to be passed in. What gets passed
///         to the actual binded function is based on the placeholders
///         specified when the 'bind' was called. The rest of the
///         arguments are 'unused' by default.
///
///         This makes it very easy to simplify the interface of a
///         function and call another object without the need to
///         care on how many arguments the bind object will be called
///         with. I saw this used with 'Invoke' and GTest/GMock.

/// @brief  A simple function that prints out the input integer.
void justAFunction(int const anInt) {
    std::cout << "The int [" << anInt << "]" << std::endl;
}

int main(int, char**) {

    using namespace std::placeholders;

    auto f1 = std::bind(justAFunction, _2);

    f1("mine", 10);

    f1(20, 30);

    f1("test", 30, 40, "another");

    return EXIT_SUCCESS;
}

