
/*!
 *  @mainpage   Pointer Arithmetic
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Demonstrate the result of performing arithmetic operations
 *  on pointer types.
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>

/*
 *  @brief      Main function
 *  @details    Create a few pointers and perform arithmetic operations
 *              on them.
 */
int main(int, char**)
{
    int x[] = {1, 4, 8, 5, 1, 4};
    int *ptr, y;

    ptr = x + 3;
    y = ptr - x;

    std::cout << "The value: " << y << std::endl;

    std::cout << "Value of ptr: " << ptr << std::endl;
    std::cout << "Value of x: " << x << std::endl;
    ptr += 1;
    std::cout << "Value of (ptr + 1): " << ptr << std::endl;
}

