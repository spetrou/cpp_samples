/*!
 *  @mainpage   Iterator invalidation
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Demonstrates the correct way to use a container iterator
 *  to remove elements without invalidating it every time you
 *  erase an element.
 */

/*!
 *  @file   main.cpp
 */
#include <iostream>
#include <list>
#include <vector>

/*!
 *  @brief      Iterate a std::list and remove items.
 *  @details    Proper way to iterate a list and remove items at the
 *              same time without having iterator invalidation issues.
 */
void iterateAndRemoveListElements(void);

/*!
 *  @brief  Main function.
 */
int main(int, char**)
{
    iterateAndRemoveListElements();
}


void iterateAndRemoveListElements(void)
{
    std::list<int> myList;

    // Add a few elements
    myList.push_back(3);
    myList.push_back(34);
    myList.push_back(235);
    myList.push_back(456);
    myList.push_back(3);
    myList.push_back(345);
    myList.push_back(67);

    std::cout << "\nBefore remove the list has the following elements."
              << std::endl;
    std::list<int>::iterator it = myList.begin();
    while(it != myList.end()) {
        std::cout << "Element [" << *it << "]" << std::endl;
        ++it;
    }

    // Now iterate the list and remove all occurances of '3'
    // without having an iterator invalidation issue
    std::cout << std::endl;
    it = myList.begin();
    while(it != myList.end()) {
        if(*it == 3) {
            std::cout << "[std::list] Removing element: ["
                      << *it << "]" << std::endl;
            myList.erase(it++);
            // The above line is equivalent to
            // it = myList.erase(it);
            // Function parameters are fully evaluated before been
            // passed as input. Thus, the 'it' iterator will be
            // incremented to the next element, and it's *previous*
            // value passed to the function, avoiding any invalidation
            // issues.
            // A more straight forward way is to assign the return
            // value of 'erase' to the 'it' iterator. The function
            // 'erase' returns a pointer to the next element from the
            // one being erased.
        } else {
            ++it;
        }
    }

    std::cout << "\nAfter remove the list has the following elements."
              << std::endl;
    it = myList.begin();
    while(it != myList.end()) {
        std::cout << "Element [" << *it << "]" << std::endl;
        ++it;
    }


    // The *WRONG* way of doing this is like below.
    // When the 'erase' takes place the 'it' iterator
    // is invalidated and thus cannot be incremented.
//    for(std::list<int>::iterator it = myList.begin();
//            it != myList.end(); ++it)
//    {
//        if(*it == 3) {
//            myList.erase(it);
//        }
//    }
}

