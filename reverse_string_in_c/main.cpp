
/*!
 *  @mainpage   Reverse a C-string
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  The effecient way to reverse a C-style string.
 *  String in C are 'char' arrays.
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>
#include <stdlib.h>
#include <string.h>

/*!
 *  @brief      Reverse a C-style string.
 *  @details    Reverse the string the normal way, keeping a
 *              temporary variable to switch two values.
 *  @param[in,out]  theString   Pointer to beginning of string.
 */
void reverseString(char* theString);

/*!
 *  @brief      Reverse a C-style string in-place.
 *  @details    Reverse the string in-place by performing three XOR
 *              operations.
 *  @param[in,out]  theString   Pointer to beginning of string.
 */
void reverseString_inplace(char* theString);

/*!
 *  @brief  Main function.
 */
int main(int argc, char** argv)
{

    char reverse[] = "The string";
    std::cout << "Before: " << reverse << std::endl;
    reverseString(reverse);
    std::cout << "After : " << reverse << std::endl;
    reverseString_inplace(reverse);
    std::cout << "Again : " << reverse << std::endl;

    char reverse_1[] = "To";
    std::cout << "Before: " << reverse_1 << std::endl;
    reverseString(reverse_1);
    std::cout << "After : " << reverse_1 << std::endl;
    reverseString_inplace(reverse_1);
    std::cout << "Again : " << reverse_1 << std::endl;

    char reverse_2[] = "T";
    std::cout << "Before: " << reverse_2 << std::endl;
    reverseString(reverse_2);
    std::cout << "After : " << reverse_2 << std::endl;
    reverseString_inplace(reverse_2);
    std::cout << "Again : " << reverse_2 << std::endl;

    return EXIT_SUCCESS;
}

void reverseString(char* theString)
{
    int stringLen = 0;

    stringLen = strlen(theString);

    if(stringLen == 0) {
        return;
    }

    char tempChar = '\0';
    int i = 0, j = 0;

    i = 0;
    j = stringLen - 1;
    while(i < j) {
        tempChar = theString[i];
        theString[i++] = theString[j];
        theString[j--] = tempChar;
    }
}

void reverseString_inplace(char* theString)
{
    int stringLen = 0;

    stringLen = strlen(theString);

    if(stringLen == 0) {
        return;
    }

    int i = 0, j = 0;

    i = 0;
    j = stringLen - 1;
    while(i < j) {
        theString[i] = theString[i] ^ theString[j];
        theString[j] = theString[i] ^ theString[j];
        theString[i] = theString[i] ^ theString[j];
        ++i;
        --j;
    }
}


