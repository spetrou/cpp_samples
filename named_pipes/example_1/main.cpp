
/*!
 *  @mainpage   Named Pipes- Example 1
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Basic example on how to use pipes. We open a pipe, which returns
 *  a file descriptor for reading, 'fd[0]', and a dile descriptor
 *  for writing, 'fd[1]'. We spawn a new thread and we write something
 *  to the pipe which is read from the other thread.
 *  The example demonstrates how you can read and write from both
 *  threads.
 */

/*!
 *  @file   main.cpp
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>

/*!
 *  @brief  Main function.
 */
int main(int argc, char** argv)
{
    int fd[2], nbytes=0;
    pid_t childpid;
    char string[] = "Hello, world!\n";
    char readbuffer[80];

    pipe(fd);

    if((childpid = fork()) == -1) {
        perror("fork");
        exit(1);
    }

    if(childpid == 0) {
        // If we don't want unidirectional communication the
        // child process can close up input side of pipe
//        close(fd[0]);

        // Send 'string' through the output side of pipe
        write(fd[1], string, (strlen(string)+1));

        // Read in a 'string' from the pipe
        nbytes = read(fd[0], readbuffer, sizeof(readbuffer));
        printf("[CHILD] Received string: %s", readbuffer);

        exit(0);
    } else {
        // If we don't want unidirectional communication the
        // parent process can close up output side of pipe
//        close(fd[1]);

        // Read in a 'string' from the pipe
        nbytes = read(fd[0], readbuffer, sizeof(readbuffer));
        printf("[PARENT] Received string: %s", readbuffer);

        // Send 'string' through the output side of pipe
        write(fd[1], string, (strlen(string)+1));
    }

    return EXIT_SUCCESS;
}

