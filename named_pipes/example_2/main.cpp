
/*!
 *  @mainpage   Named Pipes - Example 2
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Example on how you can use named pipes to redirect the standard
 *  input of one task to another. We create the pipe file descriptors
 *  and then spawn a new thread. The second thread closes the output
 *  side of the pipe and then replaces the standard input file
 *  descriptors with the one created. This will bridge the two
 *  descriptors using 'dup2'. Then we 'exec' the 'sort' function
 *  which takes input from standard input. Whatever we send through
 *  the first thread to the pipe will be sorted by 'sort' as if they
 *  were send to it through standard input. For example this is
 *  equivalent to:
 *
 *  echo -e "Hello\nworld!\ntwo" | sort
 *
 */

/*!
 *  @file   main.cpp
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>

/*!
 *  @brief  Main function.
 */
int main(int argc, char** argv)
{
    int fd[2], nbytes=0;
    pid_t childpid;
    char string[] = "Hello\nworld!\ntwo";
    char readbuffer[80];

    pipe(fd);

    if((childpid = fork()) == -1) {
        perror("fork");
        exit(1);
    }

    if(childpid == 0) {
        /* Child process closes up output side of pipe */
        close(fd[1]);

        dup2(fd[0], 0);

        execlp("sort", "sort", NULL);
    } else {
        /* Parent process closes up input side of pipe */
        close(fd[0]);

        write(fd[1], string, (strlen(string)+1));
    }

    return EXIT_SUCCESS;
}

