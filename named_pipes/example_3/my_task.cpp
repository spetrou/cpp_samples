
/*!
 *  @mainpage   Named Pipes- Example 3
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  A small task that reads from the standard input named pipe,
 *  known by the identifier '0', and prints to the standard
 *  output. This code will be 'exec' from a thread in
 *  'main.cpp' file and inherit the named pipes. The standard
 *  input pipe will be duplicated, 'dup2', to a new pipe that
 *  the other code will use to send something to this one.
 */

/*!
 *  @file   main.cpp
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>

/*!
 *  @brief  Main function.
 */
int main(int argc, char** argv)
{
    int nbytes=0;
    char readbuffer[80];

    nbytes = read(0, readbuffer, sizeof(readbuffer));
    printf("\nReceived string: %s\n", readbuffer);

    return EXIT_SUCCESS;
}

