
/*!
 *  @mainpage   Named Pipes- Example 3
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Create a pair of named pipes and then spawn a new thread.
 *  The child process will duplicate, 'dup2', the standard input
 *  pipe and then 'exec' the task 'my_task'. This task reads
 *  whatever is sent to the standard input named pipe, known
 *  by the identifier '0', and prints it to standard output.
 *  The pipes are inherited by tasks that are 'exec' and that is
 *  what enabled this code to communicate with the other task
 *  through the named pipes.
 */

/*!
 *  @file   main.cpp
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>

/*!
 *  @brief  Main function.
 */
int main(int argc, char** argv)
{
    int fd[2];
    pid_t childpid;
    char string[] = "Hello, world!\n";

    pipe(fd);

    if((childpid = fork()) == -1) {
        perror("fork");
        exit(1);
    }

    if(childpid == 0) {
        // Child process closes up output side of pipe
        close(fd[1]);

        // Duplicate (replace) the standard input pipe with the
        // one created above.
        dup2(fd[0], 0);

        execl("./my_task", "my_task", NULL);
    } else {
        // Parent process closes up input side of pipe
        close(fd[0]);

        // Write something to the pipe that will end up been
        // read from the my_task
        write(fd[1], string, (strlen(string)+1));
    }

    return EXIT_SUCCESS;
}

