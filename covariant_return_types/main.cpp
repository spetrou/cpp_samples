
/*!
 *  @mainpage   Covariant return types
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Demonstrates that virtual function can be re-implemented to
 *  return different return types. The rule is that they can
 *  return any object which is derived from the return object
 *  of the base class function.
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>
#include <cstdlib>

/*!
 *  @class  A
 *  @brief  A base class.
 */
class A {
public:
    /*!
     *  @brief  Return a pointer of base type.
     *  @return A pointer to a new A object.
     */
    virtual A* returnSomething(void) {
        return new A();
    }

    /*!
     *  @brief  Print class identifier.
     */
    virtual void name(void) {
        std::cout << "I am A" << std::endl;
    }
};

/*!
 *  @class  B
 *  @brief  A derived class.
 */
class B : public A {
public:
    /*!
     *  @brief  Reimplement function to return derived class pointer.
     *  @return A pointer to a new B object.
     */
    virtual B* returnSomething(void) {
        return new B();
    }

    /*!
     *  @brief  Print class identifier.
     */
    virtual void name(void) {
        std::cout << "I am B" << std::endl;
    }
};

/*!
 *  @brief  Main function.
 */
int main(int, char**)
{
    A anA;
    B anB;
    A& another = anB;

    // Allocate three new objects
    A* firstPtr = anA.returnSomething();
    A* secondPtr = anB.returnSomething();
    A* thirdPtr = another.returnSomething();

    // Print out their names
    firstPtr->name();
    secondPtr->name();
    thirdPtr->name();

    // Cleanup
    delete firstPtr;
    delete secondPtr;
    delete thirdPtr;

    return EXIT_SUCCESS;
}

