
/*!
 *  @mainpage   Covariant return types and contravariant member
 *              function parameters.
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Demonstrates that virtual function can be overloaded to
 *  return different return types. The rule is that they can
 *  return any object which is 'derived' from the return object
 *  type.
 *
 *  Also, we can overload the member function input parameters
 *  to accept a base object (contravariance). Be careful on
 *  how to call the overloaded function though. Inheritance
 *  can be tricky (explanation below).
 *
 *  Class 'A' has one version of 'returnSomething' that takes
 *  an input parameter of type 'Y' and returns type 'A'..
 *
 *  Class 'B' has one version of 'returnSomething' that takes
 *  an input parameter of type 'Y' and returns type 'B'. Ths is
 *  covariance on the return type.
 *
 *  Class 'C' has two versions of 'returnSomething'. One that
 *  takes an input parameter of type 'Y', and one that takes
 *  input parameter of 'X', the base class of 'Y'.
 *  When you create a new C object and assign it to an 'A'
 *  pointer, calling 'returnSomething' with 'Y' will work
 *  and call the implementation of 'B'. Calling it with an
 *  input of 'X', will not work.
 *  When a C object is used with a C pointer, you have the
 *  option to call any of the two functions, based on the
 *  type of pointer you pass in. 'X' or 'Y'.
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>
#include <cstdlib>

class X {
public:
    void name(void) {
        std::cout << "I am X" << std::endl;
    }
};
class Y : public X {
public:
    void name(void) {
        std::cout << "I am Y" << std::endl;
    }
};
class Z : public Y {
public:
    void name(void) {
        std::cout << "I am Z" << std::endl;
    }
};
/*!
 *  @class  A
 *  @brief  A base class.
 */
class A {
public:
    /*!
     *  @brief  Return a pointer of base type.
     *  @return A pointer to a new A object.
     */
    virtual A* returnSomething(Y* input) {
        std::cout << "Allocate A" << std::endl;
        return new A();
    }

    /*!
     *  @brief  Print class identifier.
     */
    virtual void name(void) {
        std::cout << "I am A" << std::endl;
    }
};

/*!
 *  @class  B
 *  @brief  A derived class.
 */
class B : public A {
public:
    /*!
     *  @brief  Reimplement function to return derived class pointer.
     *  @return A pointer to a new B object.
     */
    virtual B* returnSomething(Y* input) {
        std::cout << "Allocate B" << std::endl;
        return new B();
    }

    /*!
     *  @brief  Print class identifier.
     */
    virtual void name(void) {
        std::cout << "I am B" << std::endl;
    }
};

class C : public B {
public:
    virtual C* returnSomething(X* input) {
        std::cout << "Allocate C" << std::endl;
        return new C();
    }

    virtual void name(void) {
        std::cout << "I am C" << std::endl;
    }
};

/*!
 *  @brief  Main function.
 */
int main(int, char**)
{
    A anA;
    B anB;
    C anC;
    A& anotherA = anB;
    A& andAnotherA = anC;

    X* anX = NULL;
    Y* anY = NULL;
    Z* anZ = NULL;

    // Allocate three new objects
    A* firstPtr = anA.returnSomething(anY);
    A* secondPtr = anB.returnSomething(anY);
    A* thirdPtr = anotherA.returnSomething(anY);

    // We only have access to 'one' of the 'returnSomething'
    // implementations of object 'C' through pointer 'A'.
    A* forthPtr = andAnotherA.returnSomething(anY);
    // This will result in an error...
//    A* fifthPtr = andAnotherA.returnSomething(anX);
//
    // Although this, is fine!!
    A* fifthPtr = anC.returnSomething(anX);
    A* sixthPtr = anC.returnSomething(anY);

    // The following calls result in a different function!!!
//    A* sixthPtr = anC.B::returnSomething(anY);
//    A* sixthPtr = anC.returnSomething(anY);

    // Print out their names
    firstPtr->name();
    secondPtr->name();
    thirdPtr->name();
    forthPtr->name();
    fifthPtr->name();
    sixthPtr->name();

    // Cleanup
    delete firstPtr;
    delete secondPtr;
    delete thirdPtr;
    delete forthPtr;
    delete fifthPtr;
    delete sixthPtr;

    return EXIT_SUCCESS;
}

