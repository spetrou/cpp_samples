
/*!
 *  @mainpage   Heap Sort algorithm implementation.
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Implementation of a head sort algorithm. It prints out
 *  the steps. It is not build for performance. Mostly for
 *  understanding the steps involved and the iterations.
 *
 *  Step 1: Create a max-heap from the initial vector.
 *  Step 2: Sort the max-heap into a sorted list.
 */

/*!
 *  @file   main.cpp
 */

#include <stdlib.h>
#include <iostream>
#include <iomanip>

int heapSize = 10;

/*!
 *  @brief  Print to standard output a vector.
 *  @param[in]  a   The vector to print out.
 */
void print(int a[]) {
    for (int i = 0; i <= 9; i++) {
        std::cout << a[i] << "-";
    }
    std::cout << std::endl;
}

/*!
 *  @brief      Get the index of the parent (root) node.
 *  @details    The heap is stored as a vector of integers. This function
 *              returns the index of the parent (root) node of the given
 *              index "i".
 *  @param[in]  i   The index that we need the parent for.
 *  @return The index of the parent (root) node.
 */
int parent(const int i) {
    if(i==1)
        return 0;

    if(i%2==0)
        return ( (i / 2)-1);
    else
        return ( (i / 2));
}

/*!
 *  @brief      Get the index of the left node.
 *  @details    The heap is stored as a vector of integers. This function
 *              returns the index of the left node of the given 'root'.
 *  @param[in]  root    The root that we need the left index for.
 *  @return The index of the left node of the 'root'.
 */
int left(const int root) {
    return (2 * root) + 1;
}

/*!
 *  @brief      Get the index of the right node.
 *  @details    The heap is stored as a vector of integers. This function
 *              returns the index of the right node of the given 'root'.
 *  @param[in]  root    The root that we need the right index for.
 *  @return The index of the right node of the 'root'.
 */
int right(const int root) {
    return (2 * root) + 2;
}

/*!
 *  @brief      Moves an element of a heap to its correct position.
 *  @details    Given a vector (representation of a heap) it moves the element
 *              pointed by the index 'i' to its correct position in the heap.
 *  @param[in]  a   The heap (vector).
 *  @param[in]  i   The index of the element we want to move to its correct
 *                  position.
 */
void heapify(int a[], int i) {
    const int l = left(i);
    const int r = right(i);
    int great(0);
    std::cout << "Comparing [" << std::setw(2) << std::setfill(' ')
              << (l >= heapSize?-1:a[l]) << "] ,["
              << std::setw(2) << std::setfill(' ')
              << a[i] << "] and ["  << std::setw(2) << std::setfill(' ')
              << (r >= heapSize?-1:a[r]) << "]\t";

    if ( (l < heapSize) && (a[l] > a[i]) ) {
        great = l;
    }
    else {
        great = i;
    }

    if ( (r < heapSize) && (a[r] > a[great]) ) {
        great = r;
    }

    if (great != i) {
        std::cout << "\tSwitch ["
                  << std::setw(2) << std::setfill(' ')
                  << a[i] << "] with ["
                  << std::setw(2) << std::setfill(' ')
                  << a[great] << "]\t\t";
        print(a);
        int temp = a[i];
        a[i] = a[great];
        a[great] = temp;
        heapify(a, great);
    }
    else {
        std::cout << "\t** NO SWITCH **" << std::endl;
    }
}

/*!
 *  @brief      Create a max-heap.
 *  @details    Given a heap (represented as a vector of integers)
 *              re-arrange the elements so that it represents a
 *              max-heap.
 *  @param[in]  a   The vector to turn into a max-heap.
 */
void BuildMaxHeap(int a[]) {
    std::cout << "Building MAX-Heap..." << std::endl;
    for (int i = (heapSize - 1) / 2; i >= 0; i--) {
        heapify(a, i);
    }
}

/*!
 *  @brief      Implements the Heap Sort algorithm.
 *  @details    Performs the two steps of the Heap Sort algorithm.
 *              Step 1: Create a max-heap.
 *              Step 2: Sort the max-heap vector.
 *  @param[in]  a   The vector to perform Heap Sort on.
 */
void HeapSort(int a[]) {
    BuildMaxHeap(a);
    std::cout << "\n\nSorting MAX-Heap..." << std::endl;
    for (int i = heapSize; i > 0; i--) {
        int temp = a[0];
        a[0] = a[heapSize - 1];
        a[heapSize - 1] = temp;
        heapSize = heapSize - 1;
        heapify(a, 0);
        std::cout << std::endl;
    }

}

/*!
 *  @brief      Main function.
 *  @details    Create a random number vector and then sort it
 *              using the 'HeapSort' function.
 *  @return     The exit code.
 */
int main() {

    int arr[] = {2, 9, 3, 6, 1, 4, 5, 7, 0, 8};
    HeapSort(arr);
    print(arr);

    return EXIT_SUCCESS;
}

