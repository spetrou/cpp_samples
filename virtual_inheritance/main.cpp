
/*!
 *  @mainpage   Virtual Inheritance
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Demonstrates how virtual inheritance works. When you want to use
 *  multiple-inheritance from classes that share common base clases
 *  you need to declare them as virtual in order for only one instance
 *  of the shared base class to exist and thus make it non ambigious
 *  which instance of the base needs to be called.
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>

/*!
 *  @class  Animal
 *  @brief  Animal (common base) class.
 */
class Animal {
public:
    /*!
     *  @brief  Virtual function to print out.
     */
    virtual void eat() {
        std::cout << "Eat animal" << std::endl;
    }
};

/*!
 *  @class  Mammal
 *  @brief  Mammal class. Virtually inheriting Animal
 */
class Mammal : public virtual Animal {
public:
    /*!
     *  @brief  Virtual function to print out.
     */
    virtual void breathe() {
        std::cout << "Breath" << std::endl;
    }

    /*!
     *  @brief  Virtual function to print out.
     */
    virtual void eat() {
        std::cout << "Eat Mammal" << std::endl;
    }
};

/*!
 *  @class  WingedAnimal
 *  @brief  WingedAnimal class. Virtually inheriting Animal.
 */
class WingedAnimal : public virtual Animal {
public:
    /*!
     *  @brief  Virtual function to print out.
     */
    virtual void flap() {
        std::cout << "Flap" << std::endl;
    }
};

/*!
 *  @class  Bat
 *  @brief  A bat is still a winged mammal.
 */
class Bat : public Mammal, public WingedAnimal {
};

/*!
 *  @brief  Main function.
 */
int main(int, char**)
{
    Animal *t1 = new Animal();
    Animal *t2 = new Mammal();
    Animal *t3 = new WingedAnimal();
    Animal *t4 = new Bat();

    t1->eat();
    t2->eat();
    t3->eat();

    // Calls the inherited, overloaded 'eat' from Mammal
    // It has precedence over the 'eat' from 'Animal'
    t4->eat();

    delete t1;
    delete t2;
    delete t3;
    delete t4;
}

