
/*!
 *  @mainpage   Virtual Destructor
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Demonstrate the need for a virtual destructor.
 *  When using polymorphism is good to know when a destructor
 *  of the base/derived class is been called.
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>
#include <stdlib.h>

/*!
 *  @class  Base
 *  @brief  Base class.
 */
class Base {
public:
    ~Base() {
        std::cout << "Non-virtual Base destructor." << std::endl;
    }
};

/*!
 *  @class  Derived
 *  @brief  Derived class.
 */
class Derived : public Base {
public:
    ~Derived() {
        std::cout << "Non-virtual Derived destructor." << std::endl;
    }
};

/*!
 *  @class  Virtual Base
 *  @brief  Virtual Base class.
 */
class VBase {
public:
    virtual ~VBase() {
        std::cout << "Virtual Base destructor." << std::endl;
    }
};

/*!
 *  @class  Virtual Derived
 *  @brief  Virtual Derived class.
 */
class VDerived : public VBase {
public:
    ~VDerived() {
        std::cout << "Virtual Derived destructor." << std::endl;
    }
};

/*!
 *  @brief  Main function.
 */
int main(int, char**)
{
    Base *basePtr = new Derived();
    std::cout << "Free Base pointer of NON-virtual destructor class." << std::endl;
    delete basePtr;

    VBase *vBasePtr = new VDerived();
    std::cout << "\nFree Base pointer of virtual destructor class." << std::endl;
    delete vBasePtr;

    Derived *derivedPtr = new Derived();
    std::cout << "\nFree Derived pointer of NON-virtual destructor class." << std::endl;
    delete derivedPtr;

    VBase *vDerivedPtr = new VDerived();
    std::cout << "\nFree Derived pointer of virtual destructor class." << std::endl;
    delete vDerivedPtr;

    return EXIT_SUCCESS;
}

