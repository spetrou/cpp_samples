
/*!
 *  @mainpage   RTTI - RunTime Type Information
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Introduces how RTTI works.
 */

/*!
 *  @file   main.cpp
 */

#include <stdlib.h>
#include <iostream>
#include <sstream>

/*!
 *  @class  Base
 *  @brief  Base class.
 */
class Base {
public:
    /*!
     *  @brief  Default virtual destructor.
     */
    virtual ~Base() {}

    /*!
     *  @brief  Virtual function.
     */
    virtual void func(void) {
        std::cout << "In base" << std::endl;
    }
};

/*!
 *  @class  Derived
 *  @brief  Derived class.
 */
class Derived : public Base {
public:
    /*!
     *  @brief  Default virtual destructor.
     */
    virtual ~Derived() {}

    /*!
     *  @brief  Virtual function.
     */
    virtual void func(void) {
        std::cout << "In derived" << std::endl;
    }
};

/*!
 *  @brief      Main function.
 *  @details    Creates two classes. One of type Base and one of Derived.
 *              Then it tries to dynamically cast them and checks the
 *              return pointer. If it's not NULL then the cast succeeded
 *              which means the pointer 'is' of the type been converted to.
 *              If the pointer is NULL it means the pointer is *not* of the
 *              type been converted to.
 */
int main(int argc, char** argv)
{
    Base* theDerivedBase = new Derived();
    Base* theBaseBase = new Base();

    Derived* thenew_1 = dynamic_cast<Derived*>(theDerivedBase);

    Derived* thenew_2 = dynamic_cast<Derived*>(theBaseBase);

    if(thenew_1 == NULL) {
        std::cout << "New 1: failed" << std::endl;
    }

    if(thenew_2 == NULL) {
        std::cout << "New 2: failed" << std::endl;
    }

    delete theBaseBase;
    delete theDerivedBase;

    return EXIT_SUCCESS;
}

