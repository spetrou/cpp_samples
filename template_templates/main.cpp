
/*!
 *  @mainpage   Template templates
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Demonstrates how you can use template templates for creating
 *  derived classes (inheritance).
 *
 *  Partial specializations of classes cannot be used with this
 *  concept. For example the code below:
 *
 *  template<class T> class A<T, 5> {
 *      short x;
 *  };
 *
 *  template<template<class T> class U> class B1 { };
 *
 *  B1<A> c;
 *
 *  Although 'A' has a specialization and it matches the definition
 *  of B1's 'U' parameter (the class A<T, 5>) it will not choose it
 *  and compile. The compiler only considers the 'primary' template
 *  (non-specialized).
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>
#include <typeinfo>
using namespace std;

/*!
 *  @class  A
 *  @brief  A (base) class.
 */
template<class T, class U>
class A {
public:
    int x;
};

/*!
 *  @class  A
 *  @brief  A (base) specialized class.
 */
template<class U>
class A<int, U> {
public:
    short x;
};

/*!
 *  @class  B
 *  @brief  B (direved) class.
 */
template<template<class T, class U> class V>
class B {
public:
    V<int, char> i;
    V<char, char> j;
};


/*!
 *  @brief  Main function.
 */
int main() {
    B<A> c;
    // The declaration V<int, char> i uses the partial specialization
    // while the declaration V<char, char> j uses the primary template.
    cout << typeid(c.i.x).name() << endl;
    cout << typeid(c.j.x).name() << endl;
}

