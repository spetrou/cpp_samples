#include <iostream>

/*!
 *  @brief      Print out the binary representation of a byte stream.
 *  @details    Having a pointer and a length, print out the bits of the stream.
 *  @param[in]  byteStream      The byte stream to print out.
 *  @param[in]  size            The size of the byte stream in bytes.
 */
void print_bits(const void* byteStream, size_t size)
{
    size_t i = size;

    std::cout << "[ ";
    while(true) {
        --i;
        for(int j = 7; j >= 0; --j) {
            if(((const unsigned char *) byteStream)[i] & (0x01<<j)) {
                std::cout << "1";
            } else {
                std::cout << "0";
            }
        }
        std::cout << " ";

        if(i == 0) {
            break;
        }
    }
    std::cout << "]" << std::endl;
}

/*!
 *  @brief      Print out the hex representation of a byte stream.
 *  @details    Having a pointer and a length, print out the bytes of the stream in hex.
 *  @param[in]  byteStream      The byte stream to print out.
 *  @param[in]  size            The size of the byte stream in bytes.
 */
void print_bytes(const void *object, size_t size)
{
    size_t i = size;

    std::cout << "[ ";

    // Save the flags to restore them at the end. We don't want the
    // HEX, or other flags, to remain set on 'cout' after this function.
    std::ios_base::fmtflags storeFlags(std::cout.flags());

    while(true)
    {
        --i;
        std::cout << std::hex << std::uppercase
                  << (((const unsigned char *) object)[i] & 0xff) << " ";
        if(i == 0) {
            break;
        }
    }
    std::cout << "]" << std::endl;
    std::cout.flags(storeFlags);
}


/*!
 *  @brief      Main function.
 *  @details    Print out the binary and hex representation of various
 *              different types.
 */
int main(int, char**)
{
    double* testDouble(new double(10.0));
    float* testFloat(new float(10.0));
    int* testInt(new int(10));

    std::cout << "Size of double: " << sizeof(double) << std::endl;
    std::cout << "Size of float: " << sizeof(float) << std::endl;
    std::cout << "Size of integer: " << sizeof(int) << "\n" << std::endl;

    print_bytes(testDouble, sizeof(testDouble));
    print_bytes(testFloat, sizeof(testFloat));
    print_bytes(testInt, sizeof(testInt));

    std::cout << std::endl;

    print_bits(testDouble, sizeof(testDouble));
    print_bits(testFloat, sizeof(testFloat));
    print_bits(testInt, sizeof(testInt));

    testDouble = reinterpret_cast<double*>(testInt);

    std::cout << "Reinterpreted cast: from int to double" << std::endl;
    std::cout << "New double: " << *testDouble << std::endl;
    print_bytes(testDouble, sizeof(testDouble));
    print_bits(testDouble, sizeof(testDouble));

    std::cout << "\n" << std::endl;
    char* aChar(new char());
    unsigned char* uChar(new unsigned char());
    signed char* sChar(new signed char());

    *aChar = 'A';
    *uChar = 'A';
    *sChar = 'A';
    print_bits(aChar, sizeof(char));
    print_bits(uChar, sizeof(char));
    print_bits(sChar, sizeof(char));
    std::cout << "Normal   [" << int(*aChar) << "]\t"
              << "Unsigned [" << int(*uChar) << "]\t"
              << "Signed   [" << int(*sChar) << "]\n"
              << "Normal   [" << *aChar << "]\t"
              << "Unsigned [" << *uChar << "]\t"
              << "Signed   [" << *sChar << "]\n\n"
              << std::endl;

    std::cout << "Shift one (<< 1)" << std::endl;
    *aChar = *aChar << 1;
    *uChar = *uChar << 1;
    *sChar = *sChar << 1;
    print_bits(aChar, sizeof(char));
    print_bits(uChar, sizeof(char));
    print_bits(sChar, sizeof(char));
    std::cout << "Normal   [" << int(*aChar) << "]\t"
              << "Unsigned [" << int(*uChar) << "]\t"
              << "Signed   [" << int(*sChar) << "]\n"
              << "Normal   [" << *aChar << "]\t"
              << "Unsigned [" << *uChar << "]\t"
              << "Signed   [" << *sChar << "]\n\n"
              << std::endl;

    std::cout << "Add 100 (+= 100)" << std::endl;
    *aChar += 100;
    *uChar += 100;
    *sChar += 100;
    print_bits(aChar, sizeof(char));
    print_bits(uChar, sizeof(char));
    print_bits(sChar, sizeof(char));
    std::cout << "Normal   [" << int(*aChar) << "]\t"
              << "Unsigned [" << int(*uChar) << "]\t"
              << "Signed   [" << int(*sChar) << "]\n"
              << "Normal   [" << *aChar << "]\t"
              << "Unsigned [" << *uChar << "]\t"
              << "Signed   [" << *sChar << "]\n\n"
              << std::endl;
}

