# C++ Samples
Collection of C++ code snippets I gather every time I run into a pitfall or a nice
piece of code I would like to keep.

### Motivation
Build a collection of useful C++ snippets I can use as a reference.

### Build Details
All samples have their own Makefile that you can use to build them. They include
code that auto-generates the header depencies using the `gcc` support. In case
you are not familiar with how it works it's good to learn adout it. The dependency
files have extension `.d`. The Makefile uses the `-MMD` command to generate them.
There's some `sed` and `fmt` magic in there too. Search the internet and you'll
find the explanation if [this link](http://scottmcpeak.com/autodepend/autodepend.html)
does not exist.

### Library Requirements
Most of the examples are not using any third party libraries. Some times though I
do like to include the following libraries:

* [log4cpp](http://log4cpp.sourceforge.net/) - Used for file logging.
* [boost](http://www.boost.org/) - Collection of various libraries (recommended).

### Directory Index
* `null_class_pointer` - Calling methods on *NULL* `class` pointers.
* `slicing` - How you pass polymorphic class object to functions is important.
* `heap_sort/implementation_1` - Simple implementation of Heap Sort algorithm that
demonstrates (prints to standard output) all the steps.
* `static_initialization` - Initialization of local `static` variables.
* `templates/promotion_issue_1` - Exposes an issue with function shadowing and
parameter promotion.
* `templates/promotion_issue_2` - Exposes same issue are previous example.
* `templates/use_of_typename/` - Demonstrates the use of keyword `typename`.
* `sockets/http_connect/` - Simple Socket connection to HTTP website.
* `sockets/rot13_with_select/` - Server/client socket example that performs rotate-13
encryption to strings.
* `virtual_static` - Demonstrates how you can use static functions/variables with
inheritance.
* `reverse_string_in_c` - How to reverse a C-style string in C. Normal and in-place
implementations.
* `inheritance_default_values` - Demonstrates the priority of default function values
in inherited virtual functions.
* `stack` - Implementation of a templated stack.
* `RTTI` - Demonstrates the RTTI (RunTime Type Information) mechanism.
* `named_pipes/example_1` - Example of a unidirectional named pipe between
two threads.
* `named_pipes/example_2` - Example of a pipe redirected to the standard input file
descriptor.
* `named_pipes/example_3` - Example of a pipe duplicated (dup2) to the standard
input file descriptor.
* `template_templates` - Example on how to use template templates with class
declarations.
* `virtual_inheritance` - Example to demonstrate virtual inheritance.
* `reference_to_null` - Example on what can happen when using a reference to
a pointer variable
* `virtual_destructor` - Example on how/when you use a virtual destructor
* `sizeof_emtpy_class` - Explain how the sizeof empty class definitions works
* `stack_arithmetic` - A code snippet to see if the stack memory moves upwards or
downwards
* `push_function` - Explain how the correctly implement a queue push function
* `print_bits` - Functions to print out the binary and hex representation of a
byte stream
* `sizeof_wchar_string` - Print out the size of a wchar datatype
* `pointer_arithmetic` - Example of arithmetic opeartions on pointers
* `specialized_template_class` - Example of specialized template class
* `iterator_invalidation` - Demonstrate how to use an iterator to erase
elements from a container
* `covariant_return_types` - Example of how virtual functions can return different
types in base and derived class implementations
* `interview_codes/sum_of_pairs` - Interview question to write a program to report
if a vector of numbers has at least two pairs that sum up to a value
* `interview_codes/tortoise_and_hare` - Implement the Tortoise and Hare cycle
detection algorithm
* `interview_codes/print_tuple` - Interview question to write a function that
prints an arbitrary number of nested pairs
* `interview_codes/reverse_words` - Interview question to reverse the order of
words in a given string
* `bind_magic` - Example to demonstrate how a 'bind' result can be used
* `proper_singleton` - Implementation of a singleton that we have control over
the lifecycle of the actual concrete instance

