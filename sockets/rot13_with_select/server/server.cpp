/*!
 *  @mainpage   Rotate-13 Socket Server.
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Socket server. It accepts incoming socket connections, then receives
 *  data from them, performs rotated-by-13 and then sends them back.
 *  It uses the 'select' function to sleep and wait for the next socket
 *  that has data to read/write. By doing this we don't have to use
 *  multiple threads to send/recv.
 *
 */

/*!
 *  @file   server.cpp
 */


/* For sockaddr_in */
#include <netinet/in.h>
/* For socket functions */
#include <sys/socket.h>
/* For fcntl */
#include <fcntl.h>
/* for select */
#include <sys/select.h>

#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#define MAX_LINE 16384

/*!
 *  @brief      Perform rotate-13 encryption.
 *  @details    Givan a character as input it rotates it by 13
 *              characters and then returns it back.
 *  @param[in]  c   Character to encrypt.
 *  @return Encrypted character.
 */
char rot13_char(char c)
{
    // We don't want to use isalpha here; setting the locale would change
    // which characters are considered alphabetical.
    if ((c >= 'a' && c <= 'm') || (c >= 'A' && c <= 'M'))
        return c + 13;
    else if ((c >= 'n' && c <= 'z') || (c >= 'N' && c <= 'Z'))
        return c - 13;
    else
        return c;
}

/*!
 *  @class  fd_state
 *  @brief  Class that holds the incoming and outgoing data.
 */
struct fd_state {
    char buffer[MAX_LINE];
    size_t buffer_used;

    int writing;
    size_t n_written;
    size_t write_upto;
};

/*!
 *  @brief      Allocate and initialize a 'fd_state' object.
 *  @details    Allocates and initializes an object of structure fd_state.
 *  @return A pointer to the newly allocated object.
 */
struct fd_state * alloc_fd_state(void)
{
    struct fd_state *state = (struct fd_state*) malloc(sizeof(struct fd_state));
    if (!state)
        return NULL;
    state->buffer_used = state->n_written = state->writing =
        state->write_upto = 0;
    return state;
}

/*!
 *  @brief      De-allocate a 'fd_state' object.
 *  @details    Free the memory allocated for a 'fd_state' object.
 *  @param[in]  state   The object pointer to de-allocate.
 */
void free_fd_state(struct fd_state *state)
{
    free(state);
}

/*!
 *  @brief      Make a socket non-blocking.
 *  @details    Given a file descriptor (socket) set the non-blocking option.
 *  @param  fd  The file descriptor (socket) to set non-blocking for.
 */
void make_nonblocking(int fd)
{
    fcntl(fd, F_SETFL, O_NONBLOCK);
}

/*!
 *  @brief      Read from a file descriptor (socket).
 *  @details    Reads from a file descriptor socket, calls the rotate-13 function on
 *              each input character and stores the results into a 'fd_state' object.
 *              It returns 1 if connection was closed, -1 on error or 0 if something was
 *              received or if there was nothing to be read this time (because of
 *              non-blocking option).
 *  @param[in]      fd      The file descriptor (socket).
 *  @param[in,out]  state   The 'fd_state' object to store the input data.
 *  @return Returns 1 on connection close, -1 on error or 0 if something was received or
 *          if there was nothing this time to be read (non-blocking option).
 */
int do_read(int fd, struct fd_state *state)
{
    char buf[1024];
    int i;
    ssize_t result;
    while (1) {
        result = recv(fd, buf, sizeof(buf), 0);
        if (result <= 0)
            break;

        for (i=0; i < result; ++i)  {
            if (state->buffer_used < sizeof(state->buffer))
                state->buffer[state->buffer_used++] = rot13_char(buf[i]);
            if (buf[i] == '\n') {
                state->writing = 1;
                state->write_upto = state->buffer_used;
            }
        }
    }

    if (result == 0) {
        return 1;
    } else if (result < 0) {
        if (errno == EAGAIN)
            return 0;
        return -1;
    }

    return 0;
}

/*!
 *  @brief      Write to a file descriptor (socket).
 *  @details    Given a 'fd_state' object it writes its contents into the socket.
 *              It returns -1 on error or 0 if something was sent was there was
 *              nothing to be send this time (non-blocking option).
 *  @param[in]      fd      The file descriptor (socket).
 *  @param[in,out]  state   The 'fd_state' object to send.
 *  @return Returns -1 if erroror 0 is something was send or when there was
 *          nothing to be send this time (non-blocking option).
 */
int do_write(int fd, struct fd_state *state)
{
    while (state->n_written < state->write_upto) {
        ssize_t result = send(fd, state->buffer + state->n_written,
                state->write_upto - state->n_written, 0);
        if (result < 0) {
            if (errno == EAGAIN)
                return 0;
            return -1;
        }
        assert(result != 0);

        state->n_written += result;
    }

    if (state->n_written == state->buffer_used)
        state->n_written = state->write_upto = state->buffer_used = 0;

    state->writing = 0;

    return 0;
}

/*!
 *  @brief      Runs the server to listen to new connections.
 *  @details    Creates a new socket listener and starts accepting new connections.
 *              For each new connection it adds it to the 'select' lists and waits
 *              for incoming data. Then it encrypts them and sends them back.
 */
void run(void)
{
    int listener;
    struct fd_state *state[FD_SETSIZE];
    struct sockaddr_in sin;
    int i, maxfd;
    fd_set readset, writeset, exset;

    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = 0;
    sin.sin_port = htons(40713);

    for (i = 0; i < FD_SETSIZE; ++i)
        state[i] = NULL;

    listener = socket(AF_INET, SOCK_STREAM, 0);
    make_nonblocking(listener);

#ifndef WIN32
    {
        int one = 1;
        setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
    }
#endif

    if (bind(listener, (struct sockaddr*)&sin, sizeof(sin)) < 0) {
        perror("bind");
        return;
    }

    if (listen(listener, 16)<0) {
        perror("listen");
        return;
    }

    FD_ZERO(&readset);
    FD_ZERO(&writeset);
    FD_ZERO(&exset);

    while (1) {
        maxfd = listener;

        FD_ZERO(&readset);
        FD_ZERO(&writeset);
        FD_ZERO(&exset);

        FD_SET(listener, &readset);

        for (i=0; i < FD_SETSIZE; ++i) {
            if (state[i]) {
                if (i > maxfd)
                    maxfd = i;
                FD_SET(i, &readset);
                if (state[i]->writing) {
                    FD_SET(i, &writeset);
                }
            }
        }

        if (select(maxfd+1, &readset, &writeset, &exset, NULL) < 0) {
            perror("select");
            return;
        }

        if (FD_ISSET(listener, &readset)) {
            struct sockaddr_storage ss;
            socklen_t slen = sizeof(ss);
            int fd = accept(listener, (struct sockaddr*)&ss, &slen);
            if (fd < 0) {
                perror("accept");
            } else if (fd > FD_SETSIZE) {
                close(fd);
            } else {
                make_nonblocking(fd);
                state[fd] = alloc_fd_state();
                assert(state[fd]);/*XXX*/
            }
        }

        for (i=0; i < maxfd+1; ++i) {
            int r = 0;
            if (i == listener)
                continue;

            if (FD_ISSET(i, &readset)) {
                r = do_read(i, state[i]);
            }
            if (r == 0 && FD_ISSET(i, &writeset)) {
                r = do_write(i, state[i]);
            }
            if (r) {
                free_fd_state(state[i]);
                state[i] = NULL;
                close(i);
            }
        }
    }
}

/*!
 *  @brief      Main function.
 *  @details    Starts the server to accept incoming connections, receive data,
 *              encrypt them and return them back.
 */
int main(int c, char **v)
{
    setvbuf(stdout, NULL, _IONBF, 0);

    run();
    return 0;
}

