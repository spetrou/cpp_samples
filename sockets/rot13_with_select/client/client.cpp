/*!
 *  @mainpage   Rotate-13 Socket Client.
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Socket client. It sends a string to a server where it gets
 *  rotated-by-13 and then receives it back.
 *
 */

/*!
 *  @file   client.cpp
 */


#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

/*!
 *  @brief      Function called on errors.
 *  @details    It prints to stderr the errno and the message
 *              passed in. Then it calls 'exit' to abort.
 *  @param[in]  msg     The error message to be printed out.
 */
void error(char *msg)
{
    perror(msg);
    exit(EXIT_SUCCESS);
}

/*!
 *  @brief      Main function.
 *  @details    Opens a socket connection to the given host and port,
 *              then reads a line from standard input, sends it to the
 *              connected host and receives back a message. The message
 *              should be the rotate-13 version of the line that was
 *              read from standard input.
 */
int main(int argc, char *argv[])
{
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    char buffer[256];

    // Check input arguments count
    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }

    // Create the socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        error((char*)"ERROR opening socket");
    }

    // Get address of the host
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }

    // Copy the address to the structure
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);

    // Read and set the port number to the address structure
    portno = atoi(argv[2]);
    serv_addr.sin_port = htons(portno);

    // Try and connect
    if (connect(sockfd, (const sockaddr*)&serv_addr,
                sizeof(serv_addr)) < 0)
    {
        error((char*)"ERROR connecting");
    }

    // Read a line from standard input
    printf("Please enter the message: ");
    bzero(buffer,256);
    fgets(buffer,255,stdin);

    // Write it to the socket
    n = write(sockfd,buffer,strlen(buffer));
    if (n < 0) {
        error((char*)"ERROR writing to socket");
    }

    // Accept something back
    bzero(buffer,256);
    n = read(sockfd,buffer,255);
    if (n < 0) {
        error((char*)"ERROR reading from socket");
    }

    // Print it to standard output
    printf("%s\n",buffer);

    return EXIT_SUCCESS;
}

