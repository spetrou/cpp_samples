
/*!
 *  @mainpage   Calling function on NULL class pointers
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Calling a method on an un-initialized pointer to a
 *  class will still work. As long as these two functions
 *  do not reference any variables or the 'this' pointer
 *  it will work. Just like a 'static' call.
 *
 *  Virtual method won't work. They need a valid object.
 */

/*!
 *  @file   main.cpp
 */

#include <stdlib.h>
#include <iostream>

class testClass {
private:
    /*!
     *  @brief  Private method that prints out.
     */
    int privTest(void) { std::cout << "In the private method" << std::endl; }

public:
    /*!
     *  @brief  Public method that prints out.
     */
    int pubTest(void) { std::cout << "In the public method" << std::endl; }

    /*!
     *  @brief  Public method that call a private mentod.
     */
    int callPrivTest(void) { privTest(); }

};

int main(int argc, char **argv)
{

    testClass *myPtr = NULL;
    testClass *myPtr1;

    myPtr->pubTest();
    myPtr->callPrivTest();

    std::cout << "Ptr NULL test: " << (myPtr == NULL) << std::endl;
    std::cout << "Ptr NULL test: " << (myPtr != NULL) << std::endl;


    myPtr1->pubTest();
    myPtr1->callPrivTest();

    std::cout << "Ptr1 NULL test: " << (myPtr1 == NULL) << std::endl;
    std::cout << "Ptr1 NULL test: " << (myPtr1 != NULL) << std::endl;
    return EXIT_SUCCESS;
}
