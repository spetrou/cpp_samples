
/*!
 *  @mainpage   Stack arithmetic.
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  A way to figure out if the 'stack' memory is growing
 *  upwards or downwards.
 */

/*!
 *  @file   main.cpp
 */

#include <iostream>
#include <stdlib.h>

/*
 *  @brief      Return pointer to local variable.
 *  @details    Print out the address of a local variable for reference and
 *              also return it.
 *  @return The address of an internal integer variable.
 */
int* func(void) {
    int anInt;

    std::cout << "[In function] The address of an int: [" << &anInt << "]" << std::endl;
    return &anInt;
}

/*
 *  @brief  Main function.
 */
int main(int, char**)
{
    int anInt;
    int* funcVarAddress = NULL;

    std::cout << "[In main] The address of an int: [" << &anInt << "]" << std::endl;

    funcVarAddress = func();

    if(&anInt - funcVarAddress < 0) {
        std::cout << "Stack grows downwards." << std::endl;
    } else {
        std::cout << "Stack grows upwards." << std::endl;
    }

    return EXIT_SUCCESS;
}

