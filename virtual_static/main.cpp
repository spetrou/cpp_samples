
/*!
 *  @mainpage   Inheritance - Static functions/variables
 *  @authors    Savvas Petrou
 *
 *  @section    intro   Introduction
 *  Show an example of having static variables and functions
 *  mixed with inheritance.
 */

/*!
 *  @file   main.cpp
 */
#include <iostream>

/*!
 *  @class  Shape
 *  @brief  Shape base class.
 */
class Shape {
private:
    static int d_count;
public:
    /*!
     *  @brief      Default constructor.
     *  @details    Inrements the static Shape counter.
     */
    Shape() {
        d_count++;
    }

    /*!
     *  @brief      Default destructor.
     *  @details    Decrments the static Shape counter.
     */
    ~Shape() {
        d_count--;
    }

    /*!
     *  @brief      Print out how many shapes exist.
     *  @details    Prints to standard output how many instances of
     *              Shape we have currently.
     */
    static void printCount(void) {
        std::cout << "[Shape] : " << d_count << std::endl;
    }

};

class Circle : public Shape {
private:
    static int d_count;
public:
    /*!
     *  @brief      Default constructor.
     *  @details    Inrements the static Circle counter.
     */
    Circle() {
        d_count++;
    }

    /*!
     *  @brief      Default destructor.
     *  @details    Decrments the static Circle counter.
     */
    ~Circle() {
        d_count--;
    }

    /*!
     *  @brief      Print out how many circles exist.
     *  @details    Prints to standard output how many instances of
     *              Circle we have currently.
     */
    static void printCount(void) {
        std::cout << "[Circle] : " << d_count << std::endl;
    }
};

class Square : public Shape {
private:
    static int d_count;
public:
    /*!
     *  @brief      Default constructor.
     *  @details    Inrements the static Square counter.
     */
    Square() {
        d_count++;
    }

    /*!
     *  @brief      Default destructor.
     *  @details    Decrments the static Square counter.
     */
    ~Square() {
        d_count--;
    }

    /*!
     *  @brief      Print out how many squares exist.
     *  @details    Prints to standard output how many instances of
     *              Square we have currently.
     */
    static void printCount(void) {
        std::cout << "[Square] : " << d_count << std::endl;
    }
};

// Initialize static variables
int Shape::d_count = 0;
int Circle::d_count = 0;
int Square::d_count = 0;

/*!
 *  @brief      Main function.
 *  @details    Create a number of different shapes and print out
 *              the static counters for each shape.
 */
int main(int, char**)
{
    const int circleCount = 5;
    const int squareCount = 4;
    Shape* shapeArray[circleCount+squareCount];

    int i = 0;
    for(; i < circleCount; ++i) {
        shapeArray[i] = new Circle();
    }
    for(; i < (circleCount + squareCount); ++i) {
        shapeArray[i] = new Square();
    }

    Shape::printCount();
    Circle::printCount();
    Square::printCount();

    i = 0;
    for(; i < (circleCount + squareCount); ++i) {
        delete shapeArray[i];
    }
}

